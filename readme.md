    Kraken library
    	server
	    	1. set current kraken server address
	    	2. get current kraken server address
	    	3. test kraken server online
	    	4. restart kraken server
	    device
	    	1. get all available devices with info (kraken device info)
	    		[
				    {
				        "active_test": {
				            "application": "com.ea.android.chs.nbamobile.placeholder",
				            "args": {
				                "KrakenExitHttp": "10.86.64.79:8000/api/device/2aca08909805/test",
				                "botLoopCount": "1",
				                "botMode": "kraken",
				                "botType": "cpu",
				                "device_id": "kraken_test_device_id_1"
				            },
				            "build": null,
				            "error": null,
				            "exit": {
				                "HTTP": {
				                    "KrakenExitHttp": "10.86.64.79:8000/api/device/2aca08909805/test"
				                }
				            },
				            "id": "998681e8-c9e0-41fb-8938-d2197e11aa1c",
				            "last_updated": "2018-07-18 16:29:35",
				            "status": "RUNNING",
				            "time_created": "2018-07-18 16:29:34",
				            "time_finished": null,
				            "time_queued": null,
				            "time_started": "2018-07-18 16:29:34"
				        },
				        "alias": "Redmi 5 Plus",
				        "deviceID": "2aca08909805",
				        "firmware": "7.1.2",
				        "form_factor": "Phone",
				        "gpu": " Adreno (TM) 506",
				        "last_updated": "2018-07-18 15:40:09",
				        "long_name": "2aca08909805 - Redmi 5 Plus (7.1.2)",
				        "mac_address": "20:47:DA:E9:B8:DB",
				        "manufacturer": "Xiaomi",
				        "model": "Redmi 5 Plus",
				        "platform": "Android",
				        "pretty_name": "Redmi 5 Plus (7.1.2)",
				        "status": "ONLINE"
				    },
				    {
				        "active_test": null,
				        "alias": "MQA82",
				        "deviceID": "ee5c4d70b9ce993c38c21fa2a25eb4cc4fdff016",
				        "firmware": "11.4",
				        "form_factor": "Phone",
				        "gpu": "Apple A11",
				        "last_updated": "2018-07-18 15:45:01",
				        "long_name": "ee5c4d70b9ce993c38c21fa2a25eb4cc4fdff016 - iPhone X (11.4)",
				        "mac_address": "e4:9a:dc:82:8e:01",
				        "manufacturer": "Apple",
				        "model": "iPhone X",
				        "platform": "iOS",
				        "pretty_name": "iPhone X (11.4)",
				        "status": "ONLINE"
				    }
				]
			2. check if device is working on a test
			3. stop app test
			4. uninstall app
			5. install app
			6. start app test
			7. check if device is connected
			8. start app test with build
			9. start app test with arguments
			10. get app test result

	Kraken Console
		commands
			connect
			restart
			devices
			run
			stop
			disconnect
            exit
            help

	Kraken Script
		script sample
			{
				id = "test_nba_pvp_desync",
				build_ios = 
				{
					package = "http://xxx.ipa",
					auth = 
					{
						user = "zhangnan",
						password = "xxxxxx",
						token = "xxxxxx=",
					},
					application = "com.ea.ios.nbamobile-cn", // TODO Is this necessary if we provide a package?
				},
				build_android = 
				{
					package = "http://xxx.apk",
					auth = 
					{
						user = "zhangnan",
						password = "xxxxxx",
						token = "xxxxxx=",
					},
					application = "com.ea.android.chs.nbamobile.placeholder", // TODO Is this necessary if we provide a package?
					activity = "com.ea.game.nba.NBAMainActivity",
				},
				args = 
				{
					botType = "cpu",
					botMode = "kraken",
					botLoopCount = "1", // TODO number instead of string?
				},
				loop = 99,
			}