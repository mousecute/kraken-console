import requests

### Console Library

class Logger(object):

    def __init__(self):
        return

    def debug(self, msg, *args, **kwargs):
        return

    def info(self, msg, *args, **kwargs):
        return

    def warning(self, msg, *args, **kwargs):
        return

    def error(self, msg, *args, **kwargs):
        return

logger = Logger()

class Build(object):

    def __init__(self, address, user, password, token):
        self.address = address
        self.user = user
        self.password = password
        self.token = token

class Device(object):

    def __init__(self, json):
        self.device_id = "N/A" if not "deviceID" in json else json["deviceID"]
        self.firmware = "N/A" if not "firmware" in json else json["firmware"]
        self.platform = "N/A" if not "platform" in json else json["platform"]
        self.alias = "N/A" if not "alias" in json else json["alias"]
        self.pretty_name = "N/A" if not "pretty_name" in json else json["pretty_name"]
        self.long_name = "N/A" if not "long_name" in json else json["long_name"]

    def isAndroid(self):
        return self.platform == "Android"

    def isIOS(self):
        return self.platform == "iOS"

class Server(object):

    def __init__(self):
        self.address = "localhost"

#Utility functions
    
    def _getUrl(self, *args):
        url = self.address
        for arg in args:
            url = url + "/" + arg
        return url

    def _getApiUrl(self, *args):
        url = self.address + "/api"
        for arg in args:
            url = url + "/" + arg
        return url

    def _getDeviceUrl(self, device, *args):
        url = self.address + "/api/device/" + device.device_id
        for arg in args:
            url = url + "/" + arg
        return url

#Server functions
    
    def setAddress(self, address):
        self.address = address

    def getAddress(self):
        return self.address

    def isOnline(self):
        # send a test url
        url = self._getUrl("")
        try:
            ret = requests.get(url)
            if ret.status_code == 200:
                return True
            else:
                return False
        except:
            return False

    def restart(self):
        #TODO server side implementation
        url = self._getUrl("restart")
        try:
            requests.get(url)
        except:
            pass

    def devices(self):
        url = self._getApiUrl("devices")
        try:
            ret = requests.get(url)
            device_jsons = ret.json()
            devices = [Device(json) for json in device_jsons]
            return devices
        except:
            return []

#Device interaction functions

    def getDeviceActiveTest(self, device):
        url = self._getDeviceUrl(device, "test", "active")
        try:
            ret = requests.get(url)
            return ret.json()
        except:
            return {}

    def getDeviceActiveTestApp(self, device):
        active_test = self.getDeviceActiveTest(device)
        return active_test["application"] if "application" in active_test else None

    def isDeviceOnline(self, device):
        url = self._getDeviceUrl(device, "status")
        try:
            ret = requests.get(url)
            return ret.json() == "ONLINE"
        except:
            return False

    def isDeviceTesting(self, device):
        url = self._getDeviceUrl(device, "testing")
        try:
            ret = requests.get(url)
            return ret.json()
        except:
            return False

    def stopDeviceTest(self, device):
        #TODO server implementation bug fix
        url = self._getDeviceUrl(device, "test", "stop")
        try:
            requests.get(url)
            return
        except:
            return False

    def uninstallDeviceApp(self, device, app_id):
        url = self._getDeviceUrl(device, "app", "uninstall", app_id)
        try:
            ret = requests.get(url)
            return ret.json()
        except:
            return False

    def installDeviceApp(self, device, build):
        url = self._getDeviceUrl(device, "app", "install")
        headers = {"Content-Type" : "application/json"}
        payload = {
            "build" : build.address,
            "build_auth" : {
                "user" : build.user,
                "password" : build.password,
                "token" : build.token,
            }
        }
        try:
            ret = requests.post(url, headers = headers, json = payload)
            return ret.json()
        except:
            return False

    def startDeviceAppTest(self, device, app_id, activity, build, arguments):
        url = self._getDeviceUrl(device, "test")
        headers = {"Content-Type" : "application/json"}
        payload = {
            "application" : app_id,
            "activity" : activity,
            "exit": {"http": "KrakenExitHttp"}, # this will be passed by kraken server to app as the key of a start arg pair whose value is the address that app talks to kraken server
            "testArgs" : {},
        }
        if build != None:
            payload["build"] = build.address
            payload["build_auth"] = {
                "user": build.user,
                "password": build.password,
                "token": build.token,
            }
        if arguments != None:
            payload["args"] = arguments
        try:
            ret = requests.post(url, headers = headers, json = payload)
            return ret.json()
        except:
            return False

    def getDeviceAppTestResult(self, device):
        #TODO 
        pass

def library_test():
    test_server = Server()
    test_server.setAddress("http://10.86.64.79:8000")
    is_online = test_server.isOnline()
    if not is_online:
        return
    devices = test_server.devices()
    if (devices.count > 0 and devices[0].isAndroid()):
        device = devices[0]
        active_test = test_server.getDeviceActiveTest(device)
        is_device_online = test_server.isDeviceOnline(device)
        is_device_running = test_server.isDeviceTesting(device)
        test_server.stopDeviceTest(device)
        test_server.uninstallDeviceApp(device, "com.ea.android.chs.nbamobile.placeholder")
        test_server.uninstallDeviceApp(device, "com.ea.android.chs.nbamobile")
        test_server.installDeviceApp(device, Build(
            "http://eamc-ota.ad.ea.com/Home/Development_Build/android/NBA_MOBILE/Version/Prod/NBA_MOBILE_android-arm-clang-dev-opt_Prod_14_268723_Quick_Parent_prod.apk", "zhangnan", "XacraqBw1984!@#$2", None))
        # test_server.startDeviceAppTest(device, "com.ea.android.chs.nbamobile", "com.ea.game.nba.NBAMainActivity",
        #     Build("http://eamc-ota.ad.ea.com/Home/Development_Build/android/NBA_MOBILE/Version/Prod/NBA_MOBILE_android-arm-clang-dev-opt_Prod_14_268723_Quick_Parent_prod.apk", "zhangnan", "XacraqBw1984!@#$2", None),
        #     {"botType": "cpu", "botMode": "kraken", "botLoopCount": "1", "device_id": "kraken_test_device_id_1"})
        test_server.startDeviceAppTest(device, "com.ea.android.chs.nbamobile", "com.ea.game.nba.NBAMainActivity", None, 
            {"botType": "cpu", "botMode": "kraken", "botLoopCount": "1", "device_id": "kraken_test_device_id_1"})


### Console Commands

class Command(object):

    def __init__(self):
        pass

    def execute(self):
        pass

class Console(object):

    def __init__(self):
        pass

    